from flask import Flask, request, jsonify
from flask_cors import CORS
from final import predict
app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
app.config['DEBUG'] = False
CORS(app)


@app.route("/biz", methods=['GET'])
def speak():
    code = request.args.get('code')
    predict(code)
    # result = sen_cos(message1,message2)
    return 'ok'

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000)
