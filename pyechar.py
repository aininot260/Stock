from pyecharts.render import make_snapshot
from snapshot_selenium import snapshot
from pyecharts import options as opts
from pyecharts.charts import Bar,Line
from pyecharts.render import make_snapshot

from pyecharts.globals import ThemeType

c = (
    Line(init_opts=opts.InitOpts(theme=ThemeType.DARK))
        .add_xaxis(['1','2','3','4','5'])
        .add_yaxis("预测", [None,None,3,4,5], markpoint_opts=opts.MarkPointOpts(data=[opts.MarkPointItem(type_="min"), opts.MarkPointItem(type_="max")]),is_connect_nones=True)
        .add_yaxis("历史", [1,2,3,None,None],markpoint_opts=opts.MarkPointOpts(data=[opts.MarkPointItem(type_="min"),opts.MarkPointItem(type_="max")]),is_connect_nones=True)
        .set_global_opts(title_opts=opts.TitleOpts(title="股票趋势预测图"))
)
c.render('1.html')