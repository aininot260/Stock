import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow import keras
import shutil,os

from matplotlib.pyplot import savefig
from snapshot_selenium import snapshot
from pyecharts import options as opts
from pyecharts.charts import Bar,Line
from pyecharts.render import make_snapshot

from pyecharts.globals import ThemeType
import happybase

def predict(code):
    conn = happybase.Connection('192.168.209.129',9090)
    #获取table实例
    table = conn.table("stock")
    time=[]
    close=[]
    select={}
    for key, value in table.scan(row_start=code, row_stop=code):
        print(key, value)
        for i in value.keys():
            select[str(i, encoding='utf-8')] = str(value[i], encoding='utf-8')
        xx=sorted(select.keys())
        print(xx)
        for i in xx:
            print(i,select[i])
            close.append(float(select[i]))
            time.append(i.split(':')[-1])
    print(time)
    print(close)
    new_data = pd.DataFrame(index=range(0, len(close)), columns=['date', 'close'])
    for i in range(0, len(close)):
        new_data['date'][i] = time[i]
        new_data['close'][i] = close[i]
    new_data.index = new_data.date
    new_data.drop('date', axis=1, inplace=True)

    model = keras.models.load_model('my_model.h5')
    model.summary()

    dataset = new_data.values
    train, test = train_test_split(dataset, random_state=0)
    scaler = MinMaxScaler(feature_range=(0, 1))
    scaled_data = scaler.fit_transform(dataset)

    inputs = new_data[len(new_data) - len(test) - 60:].values
    all_ans = []
    x = [i for i in close]
    temp = [inputs[0:60, 0]]
    f = 0
    add = 0
    while f < 1100:
        f += 1
        X_test = np.array(temp)
        X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
        closing_price = model.predict(X_test)
        cha = x[add] - inputs[add]
        t = list(temp[0])
        temp = []
        add += f % 2
        add %= 4000
        for i in x[add:add + 60]:
            temp.append(i - cha / 1.5)
        temp = [temp]
        closing_price = scaler.inverse_transform(closing_price)
        all_ans.append(round(closing_price[0][0], 2))
    #print(all_ans)
    train = new_data[:len(train)]
    plt.figure(figsize=(30, 19))
    plt.plot(train['close'], label='history')
    plt.legend()
    plt.savefig("D:\\apache-tomcat-9.0.14\\webapps\\random\\images\\dashboard_full_1.jpg")
    #plt.show()

    draw1 = []
    draw2 = []
    TIME = []

    year = int(time[0].split('-')[0])
    month = int(time[0].split('-')[1])
    day = int(time[0].split('-')[-1])



    for i in train['close']:
        draw1.append(i)
        draw2.append(None)
    m=-1
    mm=50
    for i in all_ans:
        x = round(i, 2)
        if x>m:
            m=x
        if x<mm:
            mm=x
        draw2.append(round(float(x),2))
        draw1.append(None)

    for i in range(len(draw1)):
        day += 1
        if day % 6 == 0 or day % 7 == 0:
            day += 2
        if (day > 29):
            day = 1
            month += 1
        if (month > 12):
            month = 1
            year += 1
        TIME.append(str(year) + '-' + str(month) + '-' + str(day))

    c = (
        Line(init_opts=opts.InitOpts(theme=ThemeType.DARK))
            .add_xaxis(TIME)
            .add_yaxis("历史", draw1, markpoint_opts=opts.MarkPointOpts(
            data=[opts.MarkPointItem(type_="min"), opts.MarkPointItem(type_="max")]), is_connect_nones=True)
            .add_yaxis("预测", draw2, markpoint_opts=opts.MarkPointOpts(
            data=[opts.MarkPointItem(type_="min"), opts.MarkPointItem(type_="max")]), is_connect_nones=True)
            .set_global_opts(title_opts=opts.TitleOpts(title="股票趋势预测图"))
    )
    # c.render('1.html')
    make_snapshot(snapshot, c.render(), "D:\\apache-tomcat-9.0.14\\webapps\\random\\images\\dashboard_full_2.png")
    #shutil.move("./dashboard_full_2.jpg", "")
        # line = Line("股票走势图", width=1000, height=400).use_theme("dark")
    # # 如果想要两个柱状图项目摞着展示，可以设置is_stack属性，否则并排展示
    # print(len(draw1))
    # print(len(draw2))
    # line.add("历史", TIME, draw1,  mark_point=["max",'average', "min"],is_splitline_show=False)
    # line.add("预测", TIME, draw2, mark_point=['max','min'],is_splitline_show=False)
    # line.render(path='snapshot.png', pixel_ratio=3)

if __name__ == "__main__":
    predict('600000')