import numpy as np
import pandas as pd
import os
from tensorflow import keras
from sklearn.model_selection import train_test_split
import happybase
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import tensorflow as tf
from pyecharts import Line, Page, Style
from pyecharts import configure
configure(global_theme='walden')

conn = happybase.Connection('192.168.209.129',9090)
#获取table实例
#print(conn.tables())
table = conn.table("stock")
# for key, value in table.scan():
#     print(key, value)
    #print(type(key), type(value))
# for key, value in table.scan():
#     print(key, value)
code='600004'
time=[]
close=[]
select={}
for key, value in table.scan(row_start=code, row_stop=code):
    print(key, value)
    for i in value.keys():
        select[str(i, encoding='utf-8')] = str(value[i], encoding='utf-8')
    xx=sorted(select.keys())
    print(xx)
    for i in xx:
        print(i,select[i])
        close.append(float(select[i]))
        time.append(i)
print(time)
print(close)
new_data = pd.DataFrame(index=range(0, len(close)), columns=['date', 'close'])
for i in range(0, len(close)):
    new_data['date'][i] = time[i]
    new_data['close'][i] = close[i]
new_data.index = new_data.date
new_data.drop('date', axis=1, inplace=True)

model = keras.models.load_model('my_model.h5')
model.summary()

dataset = new_data.values
train, test = train_test_split(dataset, random_state=0)

scaler = MinMaxScaler(feature_range=(0, 1))
scaled_data = scaler.fit_transform(dataset)

inputs = new_data[len(new_data) - len(test) - 60:].values
# inputs = inputs.reshape(-1,1)
# inputs = scaler.transform(inputs)
all_ans = []
# close = []
# for i in new_data['close']:
#     close.append(i)
# close = scaler.transform([close])
# print(close.shape)
x = [i for i in close]
# X_test = [inputs[0:60, 0]]
temp = [inputs[0:60, 0]]

f = 0
add = 0
while f < len(new_data) - len(train):
    f += 1
    X_test = np.array(temp)
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
    closing_price = model.predict(X_test)
    cha = x[add] - inputs[add]
    t = list(temp[0])
    temp = []
    add += f % 2
    add %= 4000
    for i in x[add:add + 60]:
        temp.append(i - cha / 1.5)
    temp = [temp]
    closing_price = scaler.inverse_transform(closing_price)
    all_ans.append(round(closing_price[0][0], 2))

print(all_ans)
train = new_data[:len(train)]
valid = new_data[len(train):]
valid['Predictions'] = all_ans
plt.figure(figsize=(30, 8))
plt.plot(train['close'], label='history')
plt.plot(valid['close'], label='real')
plt.plot(valid['Predictions'], label='predict')
plt.legend()
plt.show()

# draw1 = []
# draw2 = []
# print(type(train))
# for i in train['close']:
#     print(i)
#     draw1.append(i)
#     draw2.append(None)
# m=-1
# mm=50
# for i in all_ans:
#     x = round(i, 2)
#     if x>m:
#         m=x
#     if x<mm:
#         mm=x
#     draw2.append(x)
#     draw1.append(None)
# print(m)
# print(len(draw1))
# print(len(time))
#
# print(draw1)
# print(draw2)
# print(time)
#
# print(type(time))
# line = Line("股票走势图", width=1000, height=400).use_theme("dark")
# # 如果想要两个柱状图项目摞着展示，可以设置is_stack属性，否则并排展示
# line.add("历史", time, draw1,  mark_point=["max",'average', "min"],is_splitline_show=False)
#
# line.add("预测", time, draw2, mark_point=['max','min'],is_splitline_show=False)
# line.render()
