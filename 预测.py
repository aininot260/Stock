import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow import keras

from matplotlib.pyplot import savefig
from snapshot_selenium import snapshot
from pyecharts import options as opts
from pyecharts.charts import Bar,Line
from pyecharts.render import make_snapshot
from pyecharts.globals import ThemeType


def read():
    data=pd.read_csv('test.csv',encoding='gbk')
    data.rename(columns={'股票代码':'code', '开盘价':'open', '收盘价':'close','最低价':'low','最高价':'high'}, inplace = True)
    date = data['开始时间']
    data['date'] = pd.to_datetime(data.开始时间,format='%Y-%m-%d')
    data.index = data['date']

    d = data.sort_index(ascending=True, axis=0)
    #不修改源数据
    new_data = pd.DataFrame(index=range(0,len(data)),columns=['date', 'close'])
    #赋值
    for i in range(0,len(d)):
        new_data['date'][i] = d['date'][i]
        new_data['close'][i] = d['close'][i]
    #date=new_data.date
    #这个df的行索引是date，所以在列中把date删除了
    new_data.index = new_data.date
    new_data.drop('date', axis=1, inplace=True)
    #二维array数组
    dataset = new_data.values
    train,test=train_test_split(dataset,random_state=0)

    #数据归一化，压缩到0-1范围内
    scaler = MinMaxScaler(feature_range=(0, 1))
    scaled_data = scaler.fit_transform(dataset)
    x_train, y_train = [], []

    #构造训练数据集，用60天的数据当训练集，用下一天的数据当结果
    for i in range(60,len(train)):
        x_train.append(scaled_data[i-60:i,0])
        y_train.append(scaled_data[i,0])

    x_train, y_train = np.array(x_train), np.array(y_train)
    #调整数据格式为lstm需要的，batch_size，sequence_length,embedding_dim
    x_train = np.reshape(x_train, (x_train.shape[0],x_train.shape[1],1))
    return new_data,scaler,train,test,date

def load_model():
    model = keras.models.load_model('my_model.h5')
    model.summary()
    return model

def predict(new_data,train,test,model,scaler):
    inputs = new_data[len(new_data) - len(test) - 60:].values
    # inputs = inputs.reshape(-1,1)
    inputs = scaler.transform(inputs)
    all_ans = []
    close = []
    for i in new_data['close']:
        close.append(i)
    close = scaler.transform([close])
    print(close.shape)
    x = [i for i in close[0]]
    X_test = [inputs[0:60, 0]]
    temp = [inputs[0:60, 0]]

    f = 0
    add = 0
    while f < 1100:
        f += 1
        X_test = np.array(temp)
        X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
        closing_price = model.predict(X_test)
        cha = x[add] - inputs[add]
        t = list(temp[0])
        temp = []
        add += f % 2
        add %= 4000
        for i in x[add:add + 60]:
            temp.append(i - cha / 1.5)
        temp = [temp]
        closing_price = scaler.inverse_transform(closing_price)
        all_ans.append(round(closing_price[0][0],2))
    return all_ans

def show(train,test,new_data,all_ans):

    train = new_data[:len(train)]
    # valid = new_data[len(train):]
    # valid['Predictions'] = all_ans
    # plt.plot(valid['close'],label='real')
    # plt.plot(valid['Predictions'],label='predict')

    draw1 = []
    draw2 = []
    print(type(train))
    time=[]
    n=0
    for i in train['close']:
        #print(i)
        draw1.append(i)
        draw2.append(None)
        time.append(n)
        n=n+1
    m=-1
    mm=50
    for i in all_ans:
        #print(type(i))
        x = round(i, 2)
        if x>m:
            m=x
        if x<mm:
            mm=x
        draw2.append(round(float(x),2))
        draw1.append(None)
        time.append(n)
        n = n + 1

    plt.figure(figsize=(30, 8))
    plt.plot(train['close'], label='history')
    plt.legend()
    plt.savefig("./WebRoot/images/history.jpg")

    plt.show()
    TIME=[]

    print(type(date))

    year=int(date[0].split('-')[0])
    month=int(date[0].split('-')[1])
    day=int(date[0].split('-')[-1])
    for i in time:

        day+=1
        if day%6==0 or day%7==0:
            day+=2
        if(day>29):
            day=1
            month+=1
        if(month>12):
            month=1
            year+=1
        TIME.append(str(year)+'-'+str(month)+'-'+str(day))
    # line = Line("股票走势图", width=1000, height=400).use_theme("dark")
    # # 如果想要两个柱状图项目摞着展示，可以设置is_stack属性，否则并排展示
    # line.add("历史", TIME, draw1,  mark_point=["max",'average', "min"],is_splitline_show=False)
    #
    # line.add("预测",TIME, draw2, mark_point=['max','min'],is_splitline_show=False)
    # line.render(path='snapshot.jpeg', pixel_ratio=3)
    # line = Line()
    # # 如果想要两个柱状图项目摞着展示，可以设置is_stack属性，否则并排展示
    # line.add("历史", TIME, draw1, mark_point=["max", 'average', "min"], is_splitline_show=False)
    # line.add("预测", TIME, draw2, mark_point=['max', 'min'], is_splitline_show=False)
    # line.render(path='snapshot.jpeg', pixel_ratio=3)

    # print(draw2)
    # print(len(TIME))
    # print(len(draw2))
    c = (
        Line(init_opts=opts.InitOpts(theme=ThemeType.DARK))
            .add_xaxis(TIME)
            .add_yaxis("历史", draw1,markpoint_opts=opts.MarkPointOpts(data=[opts.MarkPointItem(type_="min"),opts.MarkPointItem(type_="max")]),is_connect_nones=True)
            .add_yaxis("预测", draw2,markpoint_opts=opts.MarkPointOpts(data=[opts.MarkPointItem(type_="min"),opts.MarkPointItem(type_="max")]), is_connect_nones=True)
            .set_global_opts(title_opts=opts.TitleOpts(title="股票趋势预测图"))
    )
    #c.render('1.html')
    make_snapshot(snapshot, c.render(), "predict.png")

if __name__ == '__main__':
    new_data, scaler, train, test,date=read()
    model=load_model()
    all_ans=predict(new_data,train,test,model,scaler)
    show(train, test, new_data, all_ans)
